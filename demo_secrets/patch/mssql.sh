#!/bin/bash
# usage: file_env VAR [DEFAULT]
#    ie: file_env 'XYZ_DB_PASSWORD' 'example'
# (will allow for "$XYZ_DB_PASSWORD_FILE" to fill in the value of
#  "$XYZ_DB_PASSWORD" from a file, especially for Docker's secrets feature)
file_env() {
	local var="$1"
	local fileVar="${var}_FILE"
	local def="${2:-}"
	if [ "${!var:-}" ] && [ "${!fileVar:-}" ]; then
		#mysql_error "Both $var and $fileVar are set (but are exclusive)"
		echo "Both $var and $fileVar are set (but are exclusive)"
		exit -1
	fi
	local val="$def"
	if [ "${!var:-}" ]; then
		val="${!var}"
	elif [ "${!fileVar:-}" ]; then
		val="$(< "${!fileVar}")"
	fi
	export "$var"="$val"
	unset "$fileVar"
}


# Generic method looping in all variables.
# Meaning that _FILE suffix in a variable must be used only in this context.
for VAR_NAME_FILE in $(env | cut -f1 -d= | grep '_FILE$'); do
  if [[ -n "$VAR_NAME_FILE" ]]; then
    file_env "${VAR_NAME_FILE%_FILE}"
  fi
done
echo "Start Mssql"
/opt/mssql/bin/nonroot_msg.sh /opt/mssql/bin/sqlservr "$@"
